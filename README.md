# Universal mocking for Cypress with MSW

## Idea
Use a cooke named msw-mocks to supply the mock files you want to use to either msw/worker (client) and msw/node (server).
The idea is to be able to have a single set-up for deserializing the cookies to MSW handlers.

![Image of Yaktocat](https://i.ibb.co/0MBtsYr/IMG-20200923-134055.jpg)