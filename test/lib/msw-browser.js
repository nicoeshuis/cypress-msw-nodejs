// src/mocks/browser.js
import { setupWorker } from 'msw';
import {getMockMapFromCookieString, parseBrowserCookieValue, updateMswInterceptorWithMockMap} from "./helpers";
import fixtures from "../mocks";

export function setUpBrowserMocking() {
    const parsedCookies = parseBrowserCookieValue(document.cookie);
    const mswRequestInterceptor = setupWorker(...fixtures.map(fixture => fixture.defaultHandler()));

    if(parsedCookies['msw-mocks']) {
        const mockMap = getMockMapFromCookieString(parsedCookies['msw-mocks']);
        updateMswInterceptorWithMockMap(mswRequestInterceptor, mockMap);
    }

    mswRequestInterceptor.start();
}