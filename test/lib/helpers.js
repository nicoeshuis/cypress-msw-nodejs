import path from "path";
import API_TVMAZE_SHOW_EPISODES from "../mocks/api-tvmaze-show-episodes";

export function handlerReturnJson(jsonFileContents) {
    return (req, res, ctx) => {
        return res(
            ctx.json(jsonFileContents),
        )
    }
}
/**
 *
 * @param options
 * @returns {
 *  name: string
 *  defaultHandler: () => RequestHandler<MockedRequest<DefaultRequestBodyType>
 *  overrideHandler: (fileName) => RequestHandler<MockedRequest<DefaultRequestBodyType>
 * }
 */
export function createMockModule(options) {
    validateMockModuleOptions(options);
    return {
        name: options.name,
        fixtures: options.fixtures,
        defaultHandler: () => options.handler(options.fixtureNames.DEFAULT, options.fixtures),
        overrideHandler: (fixtureName) => options.handler(fixtureName, options.fixtures)
    };
}

function validateMockModuleOptions(options) {
    ['name', 'fixtures', 'fixtureNames', 'handler'].forEach(item => {
        if(!options[item]) {
            throw new Error(`${item} should be defined`)
        }
    });

    if(!options.fixtures.default) {
        throw new Error('Mock module should contain a fixture with key "default"');
    }
}

export function getMockMapFromCookieString(cookieString) {
    const mswMockStrings = cookieString.split('&');
    return mswMockStrings.reduce((acc, val) => {
        const keyValuePair = val.split('__');
        acc[keyValuePair[0]] = keyValuePair[1];
        return acc;
    }, {});
}

export function parseBrowserCookieValue(browserCookieValue) {
    return browserCookieValue
        .split(';')
        .reduce((res, c) => {
            const [key, val] = c.trim().split('=').map(decodeURIComponent);
            const allNumbers = str => /^\d+$/.test(str);
            try {
                return Object.assign(res, { [key]: allNumbers(val) ?  val : JSON.parse(val) })
            } catch (e) {
                return Object.assign(res, { [key]: val })
            }
        }, {});
}

/**
 * Update the mocks with the files from the mockMap
 */
export function updateMswInterceptorWithMockMap(mswRequestInterceptor, mockMap) {
    console.log('Updating request interceptor with mocks', mockMap);
    mswRequestInterceptor.use(API_TVMAZE_SHOW_EPISODES.overrideHandler(mockMap[API_TVMAZE_SHOW_EPISODES.name]));
}