module.exports = {
  "id": 6770,
  "url": "http://www.tvmaze.com/shows/6770/the-lion-guard",
  "name": "The Lion Guard",
  "type": "Animation",
  "language": "English",
  "genres": [
    "Adventure",
    "Children"
  ],
  "status": "To Be Determined",
  "runtime": 22,
  "premiered": "2016-01-15",
  "officialSite": "https://disneynow.com/shows/the-lion-guard",
  "schedule": {
    "time": "09:00",
    "days": [
      "Saturday"
    ]
  },
  "rating": {
    "average": 5
  },
  "weight": 79,
  "network": null,
  "webChannel": {
    "id": 83,
    "name": "DisneyNOW",
    "country": {
      "name": "United States",
      "code": "US",
      "timezone": "America/New_York"
    }
  },
  "externals": {
    "tvrage": null,
    "thetvdb": 282756,
    "imdb": "tt3793630"
  },
  "image": {
    "medium": "http://static.tvmaze.com/uploads/images/medium_portrait/25/64005.jpg",
    "original": "http://static.tvmaze.com/uploads/images/original_untouched/25/64005.jpg"
  },
  "summary": "<p><b>The Lion Guard</b> will recount the efforts of Kion, the son and youngest child of Simba and Nala, to assemble a team of animals to protect the Pride Lands. Some characters from <i>The Lion King</i>, its 1998 sequel <i>The Lion King II: Simba's Pride</i> and 2004's <i>The Lion King 1½</i>, will appear such as Kiara, Timon, Pumbaa, Rafiki, Zazu, Simba, Nala and Mufasa. It will also feature new characters including a cheetah named Fuli, a hippopotamus named Beshte, an egret named Ono and a honey badger named Bunga, who will serve as Kion's friends.</p>",
  "updated": 1580335023,
  "_links": {
    "self": {
      "href": "http://api.tvmaze.com/shows/6770"
    },
    "previousepisode": {
      "href": "http://api.tvmaze.com/episodes/1717014"
    }
  },
  "_embedded": {
    "episodes": [
      {
        "id": 537590,
        "url": "http://www.tvmaze.com/episodes/537590/the-lion-guard-1x01-never-judge-a-hyena-by-its-spots",
        "name": "Never Judge a Hyena by Its Spots",
        "season": 1,
        "number": 1,
        "airdate": "2016-01-15",
        "airtime": "09:30",
        "airstamp": "2016-01-15T14:30:00+00:00",
        "runtime": 25,
        "image": null,
        "summary": "<p>Kion gets separated from the rest of the Lion Guard and meets Jasiri in the Outlands who rescues and helps him.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/537590"
          }
        }
      },
      {
        "id": 547153,
        "url": "http://www.tvmaze.com/episodes/547153/the-lion-guard-1x02-the-rise-of-makuu",
        "name": "The Rise of Makuu",
        "season": 1,
        "number": 2,
        "airdate": "2016-01-15",
        "airtime": "10:00",
        "airstamp": "2016-01-15T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Makuu takes over as a leader of the crocodiles; Kion questions whether to intervene or respect animal traditions.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/547153"
          }
        }
      },
      {
        "id": 547154,
        "url": "http://www.tvmaze.com/episodes/547154/the-lion-guard-1x03-bunga-the-wise",
        "name": "Bunga the Wise",
        "season": 1,
        "number": 3,
        "airdate": "2016-01-22",
        "airtime": "09:30",
        "airstamp": "2016-01-22T14:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Floodwaters threaten the Pride Lands; Bunga's quick fix may not be the best solution.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/547154"
          }
        }
      },
      {
        "id": 547155,
        "url": "http://www.tvmaze.com/episodes/547155/the-lion-guard-1x04-cant-wait-to-be-queen",
        "name": "Can't Wait to be Queen",
        "season": 1,
        "number": 4,
        "airdate": "2016-01-29",
        "airtime": "09:30",
        "airstamp": "2016-01-29T14:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Simba leaves Kiara in charge of the Pride Lands while he goes to say goodbye to an old elephant friend named Amanifu who has just passed away. Upon learning of this from Mzingo, Janja decides to take advantage of Kiara's inexperience and comes up with a plan to take over the Pride Lands.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/547155"
          }
        }
      },
      {
        "id": 579904,
        "url": "http://www.tvmaze.com/episodes/579904/the-lion-guard-1x05-eye-of-the-beholder",
        "name": "Eye of the Beholder",
        "season": 1,
        "number": 5,
        "airdate": "2016-02-05",
        "airtime": "09:30",
        "airstamp": "2016-02-05T14:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>While helping the Lion Guard driving away Janja's Clan following their attempted attack on a wildebeest herd, Ono temporarily loses his vision in his left eye. When Janja overhears the news from the vultures, he plans to take advantage of this. Meanwhile, Rafiki works on his paintings of the Lion Guard.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/579904"
          }
        }
      },
      {
        "id": 579905,
        "url": "http://www.tvmaze.com/episodes/579905/the-lion-guard-1x06-the-kupatana-celebration",
        "name": "The Kupatana Celebration",
        "season": 1,
        "number": 6,
        "airdate": "2016-02-12",
        "airtime": "09:30",
        "airstamp": "2016-02-12T14:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>During a celebrated holiday in the Pride Lands, Kion and his friends rescue a jackal pup from Janja but soon discover that he is in a family of jackals and the matriarch, Reirei, manages to fool Kion into letting them stay.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/579905"
          }
        }
      },
      {
        "id": 579906,
        "url": "http://www.tvmaze.com/episodes/579906/the-lion-guard-1x07-fulis-new-family",
        "name": "Fuli's New Family",
        "season": 1,
        "number": 7,
        "airdate": "2016-02-19",
        "airtime": "09:30",
        "airstamp": "2016-02-19T14:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>The gang feel as though that Fuli has no family and invite her to join their individual activities, oblivious to the fact that she enjoys flying solo. Meanwhile, Bunga lets immunity go to his head after being told that he is immune to Ushari's venom.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/579906"
          }
        }
      },
      {
        "id": 579908,
        "url": "http://www.tvmaze.com/episodes/579908/the-lion-guard-1x08-the-search-for-utamu",
        "name": "The Search for Utamu",
        "season": 1,
        "number": 8,
        "airdate": "2016-02-26",
        "airtime": "09:30",
        "airstamp": "2016-02-26T14:30:00+00:00",
        "runtime": 30,
        "image": {
          "medium": "http://static.tvmaze.com/uploads/images/medium_landscape/46/116730.jpg",
          "original": "http://static.tvmaze.com/uploads/images/original_untouched/46/116730.jpg"
        },
        "summary": "<p>Bunga, Kion, Ono, and Beshte search for Utamu grubs. On the way, Bunga tells the story of how he met Timon and Pumbaa. Meanwhile, Fuli goes on a mission alone and overexerts herself leaving her vulnerable to an attack by Mzingo's flock.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/579908"
          }
        }
      },
      {
        "id": 632821,
        "url": "http://www.tvmaze.com/episodes/632821/the-lion-guard-1x09-follow-that-hippo",
        "name": "Follow that Hippo",
        "season": 1,
        "number": 9,
        "airdate": "2016-03-18",
        "airtime": "09:30",
        "airstamp": "2016-03-18T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Beshte rescues a young elephant that idolizes him after he wanders into danger.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/632821"
          }
        }
      },
      {
        "id": 632823,
        "url": "http://www.tvmaze.com/episodes/632823/the-lion-guard-1x10-the-call-of-the-drongo",
        "name": "The Call of the Drongo",
        "season": 1,
        "number": 10,
        "airdate": "2016-03-25",
        "airtime": "09:30",
        "airstamp": "2016-03-25T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>The Lion Guard steps in when they encounter a deceptive drongo bird named Tamaa who mimics predators to scare off smaller animals.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/632823"
          }
        }
      },
      {
        "id": 664718,
        "url": "http://www.tvmaze.com/episodes/664718/the-lion-guard-1x11-paintings-and-predictions",
        "name": "Paintings and Predictions",
        "season": 1,
        "number": 11,
        "airdate": "2016-04-01",
        "airtime": "09:30",
        "airstamp": "2016-04-01T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Bunga thinks that Rafiki's paintings can predict the future and believes that Kion will fall from a high tree. Soon, he and the rest of the guard desperately try to keep Kion from climbing trees. Meanwhile, Janja, Cheezi, and Chungu plan an attack on the zebra herd.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/664718"
          }
        }
      },
      {
        "id": 664719,
        "url": "http://www.tvmaze.com/episodes/664719/the-lion-guard-1x12-the-mbali-fields-migration",
        "name": "The Mbali Fields Migration",
        "season": 1,
        "number": 12,
        "airdate": "2016-04-22",
        "airtime": "09:30",
        "airstamp": "2016-04-22T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/664719"
          }
        }
      },
      {
        "id": 664720,
        "url": "http://www.tvmaze.com/episodes/664720/the-lion-guard-1x13-bunga-and-the-king",
        "name": "Bunga and the King",
        "season": 1,
        "number": 13,
        "airdate": "2016-04-29",
        "airtime": "09:30",
        "airstamp": "2016-04-29T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>When Simba and Bunga get trapped in a sinkhole, it's up to the Lion Guard to save them. Meanwhile, Simba and Bunga realize that being raised by Timon and Pumbaa is something that they have in common.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/664720"
          }
        }
      },
      {
        "id": 778985,
        "url": "http://www.tvmaze.com/episodes/778985/the-lion-guard-1x14-the-imaginary-okapi",
        "name": "The Imaginary Okapi",
        "season": 1,
        "number": 14,
        "airdate": "2016-07-08",
        "airtime": "09:30",
        "airstamp": "2016-07-08T13:30:00+00:00",
        "runtime": 30,
        "image": {
          "medium": "http://static.tvmaze.com/uploads/images/medium_landscape/66/166739.jpg",
          "original": "http://static.tvmaze.com/uploads/images/original_untouched/66/166739.jpg"
        },
        "summary": "<p>Beshte makes friends with a timid okapi named Ajabu who has traveled to the Pride Lands to get away from Makucha the leopard, but the guard thinks that Ajabu is imaginary due to his tendency to hide from others. When Makucha shows up in the Pride Lands however, the guard soon realizes that Beshte's new friend is real.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/778985"
          }
        }
      },
      {
        "id": 831449,
        "url": "http://www.tvmaze.com/episodes/831449/the-lion-guard-1x15-too-many-termites",
        "name": "Too Many Termites",
        "season": 1,
        "number": 15,
        "airdate": "2016-07-15",
        "airtime": "09:30",
        "airstamp": "2016-07-15T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>The Lion Guard mistakenly chase off aardwolves instead of hyenas and the Pride Lands becomes overrun with termites.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/831449"
          }
        }
      },
      {
        "id": 831450,
        "url": "http://www.tvmaze.com/episodes/831450/the-lion-guard-1x16-the-trouble-with-galagos",
        "name": "The Trouble with Galagos",
        "season": 1,
        "number": 16,
        "airdate": "2016-08-05",
        "airtime": "09:30",
        "airstamp": "2016-08-05T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>The Guard must help a leopard stand up to a bully challenging his territory.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/831450"
          }
        }
      },
      {
        "id": 831451,
        "url": "http://www.tvmaze.com/episodes/831451/the-lion-guard-1x17-janjas-new-crew",
        "name": "Janja's New Crew",
        "season": 1,
        "number": 17,
        "airdate": "2016-08-26",
        "airtime": "09:30",
        "airstamp": "2016-08-26T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Janja replaces Chungu and Cheezi with two new hyenas in an attempt to sideline the Lion Guard.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/831451"
          }
        }
      },
      {
        "id": 906198,
        "url": "http://www.tvmaze.com/episodes/906198/the-lion-guard-1x18-baboons",
        "name": "Baboons!",
        "season": 1,
        "number": 18,
        "airdate": "2016-09-23",
        "airtime": "09:30",
        "airstamp": "2016-09-23T13:30:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": "<p>After the Lion Guard rescue a baby baboon, Fuli is charged with returning the baboon to its mother.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/906198"
          }
        }
      },
      {
        "id": 929912,
        "url": "http://www.tvmaze.com/episodes/929912/the-lion-guard-1x19-beware-the-zimwi",
        "name": "Beware the Zimwi",
        "season": 1,
        "number": 19,
        "airdate": "2016-10-14",
        "airtime": "09:30",
        "airstamp": "2016-10-14T13:30:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": "<p>When tales of a mysterious creature plague the Pride Lands, the Guard must conquer their fears and track down the elusive beast.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/929912"
          }
        }
      },
      {
        "id": 961188,
        "url": "http://www.tvmaze.com/episodes/961188/the-lion-guard-1x20-lions-of-the-outlands",
        "name": "Lions of the Outlands",
        "season": 1,
        "number": 20,
        "airdate": "2016-11-11",
        "airtime": "09:30",
        "airstamp": "2016-11-11T14:30:00+00:00",
        "runtime": 25,
        "image": null,
        "summary": "<p>When Jasiri asks the Lion Guard for help, they must choose whether to side with the hyenas or the lions.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/961188"
          }
        }
      },
      {
        "id": 979425,
        "url": "http://www.tvmaze.com/episodes/979425/the-lion-guard-1x21-never-roar-again",
        "name": "Never Roar Again",
        "season": 1,
        "number": 21,
        "airdate": "2016-11-19",
        "airtime": "09:30",
        "airstamp": "2016-11-19T14:30:00+00:00",
        "runtime": 25,
        "image": null,
        "summary": "<p>Fearing he's lost control of his roar, Kion vows to never use it again.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/979425"
          }
        }
      },
      {
        "id": 989940,
        "url": "http://www.tvmaze.com/episodes/989940/the-lion-guard-1x22-the-lost-gorillas",
        "name": "The Lost Gorillas",
        "season": 1,
        "number": 22,
        "airdate": "2016-12-02",
        "airtime": "09:30",
        "airstamp": "2016-12-02T14:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>The Guard must help two zany gorilla princes return to their home in the snowy Rwenzori Mountains</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/989940"
          }
        }
      },
      {
        "id": 1013791,
        "url": "http://www.tvmaze.com/episodes/1013791/the-lion-guard-1x23-the-trail-to-udugu",
        "name": "The Trail to Udugu",
        "season": 1,
        "number": 23,
        "airdate": "2017-01-06",
        "airtime": "10:30",
        "airstamp": "2017-01-06T15:30:00+00:00",
        "runtime": 25,
        "image": {
          "medium": "http://static.tvmaze.com/uploads/images/medium_landscape/92/230697.jpg",
          "original": "http://static.tvmaze.com/uploads/images/original_untouched/92/230697.jpg"
        },
        "summary": "<p>While Nala takes Kion and Kiara on a trip to teach them the importance of working together, Simba is left in charge of the Lion Guard.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1013791"
          }
        }
      },
      {
        "id": 1051577,
        "url": "http://www.tvmaze.com/episodes/1051577/the-lion-guard-1x24-onos-idol",
        "name": "Ono's Idol",
        "season": 1,
        "number": 24,
        "airdate": "2017-02-24",
        "airtime": "10:00",
        "airstamp": "2017-02-24T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>When Ono meets his idol, the legendary eagle Hadithi, he learns that his hero isn't what he expected.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1051577"
          }
        }
      },
      {
        "id": 1080766,
        "url": "http://www.tvmaze.com/episodes/1080766/the-lion-guard-1x25-beshte-and-the-hippo-lanes",
        "name": "Beshte and the Hippo Lanes",
        "season": 1,
        "number": 25,
        "airdate": "2017-03-17",
        "airtime": "09:30",
        "airstamp": "2017-03-17T13:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1080766"
          }
        }
      },
      {
        "id": 1108121,
        "url": "http://www.tvmaze.com/episodes/1108121/the-lion-guard-1x26-ono-the-tickbird",
        "name": "Ono the Tickbird",
        "season": 1,
        "number": 26,
        "airdate": "2017-04-21",
        "airtime": "11:30",
        "airstamp": "2017-04-21T15:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Ono must fill in for a rhino‘s \"seeing-eye bird\" while the Guard helps the rhino and tickbird resolve their differences.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1108121"
          }
        }
      },
      {
        "id": 1235676,
        "url": "http://www.tvmaze.com/episodes/1235676/the-lion-guard-2x01-babysitter-bunga",
        "name": "Babysitter Bunga",
        "season": 2,
        "number": 1,
        "airdate": "2017-07-07",
        "airtime": "08:00",
        "airstamp": "2017-07-07T12:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>When Muhimu the zebra asks Bunga to babysit, his unorthodox style soon catches on, and he is put in charge of several more young Pride Landers.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1235676"
          }
        }
      },
      {
        "id": 1235675,
        "url": "http://www.tvmaze.com/episodes/1235675/the-lion-guard-2x02-savannah-summit",
        "name": "Savannah Summit",
        "season": 2,
        "number": 2,
        "airdate": "2017-07-07",
        "airtime": "08:30",
        "airstamp": "2017-07-07T12:30:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>While Simba gathers the animal leaders for a Savannah Summit, the Lion Guard learns of a plot against Twiga the giraffe.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1235675"
          }
        }
      },
      {
        "id": 1235677,
        "url": "http://www.tvmaze.com/episodes/1235677/the-lion-guard-2x03-the-traveling-baboon-show",
        "name": "The Traveling Baboon Show",
        "season": 2,
        "number": 3,
        "airdate": "2017-07-14",
        "airtime": "08:00",
        "airstamp": "2017-07-14T12:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>The Lion Guard must work together to chase away a baboon comedy troupe that has been distracting the other animals with their antics and stealing their food.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1235677"
          }
        }
      },
      {
        "id": 1239690,
        "url": "http://www.tvmaze.com/episodes/1239690/the-lion-guard-2x04-ono-and-the-egg",
        "name": "Ono and the Egg",
        "season": 2,
        "number": 4,
        "airdate": "2017-07-21",
        "airtime": "08:00",
        "airstamp": "2017-07-21T12:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Ono begrudgingly becomes the protector of a cuckoo bird's egg left in his nest and chases off an African harrier hawk who has come to the Pride Lands to hunt for tasty new treats.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1239690"
          }
        }
      },
      {
        "id": 1239691,
        "url": "http://www.tvmaze.com/episodes/1239691/the-lion-guard-2x05-the-rise-of-scar",
        "name": "The Rise of Scar",
        "season": 2,
        "number": 5,
        "airdate": "2017-07-29",
        "airtime": "09:00",
        "airstamp": "2017-07-29T13:00:00+00:00",
        "runtime": 60,
        "image": null,
        "summary": "<p>After using the Roar of the Elders in anger, Kion unwittingly summons the Pride Lands' greatest villain, Scar.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1239691"
          }
        }
      },
      {
        "id": 1256749,
        "url": "http://www.tvmaze.com/episodes/1256749/the-lion-guard-2x06-let-sleeping-crocs-lie",
        "name": "Let Sleeping Crocs Lie",
        "season": 2,
        "number": 6,
        "airdate": "2017-08-11",
        "airtime": "08:00",
        "airstamp": "2017-08-11T12:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>After the Lion Guard accidentally wakes a group of hibernating crocodiles who become furious that they now have to suffer through the dry season in the Savanna, Scar seizes the opportunity to wreak havoc in the Pride Lands.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1256749"
          }
        }
      },
      {
        "id": 1300532,
        "url": "http://www.tvmaze.com/episodes/1300532/the-lion-guard-2x07-swept-away",
        "name": "Swept Away",
        "season": 2,
        "number": 7,
        "airdate": "2017-09-15",
        "airtime": "11:00",
        "airstamp": "2017-09-15T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": "<p>Beshte ends up alone in the Outlands and Scar uses the opportunity to attack the Lion Guard's strongest member.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1300532"
          }
        }
      },
      {
        "id": 1300864,
        "url": "http://www.tvmaze.com/episodes/1300864/the-lion-guard-2x08-rafikis-new-neighbors",
        "name": "Rafiki's New Neighbors",
        "season": 2,
        "number": 8,
        "airdate": "2017-09-22",
        "airtime": "11:00",
        "airstamp": "2017-09-22T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1300864"
          }
        }
      },
      {
        "id": 1300868,
        "url": "http://www.tvmaze.com/episodes/1300868/the-lion-guard-2x09-rescue-in-the-outlands",
        "name": "Rescue in the Outlands",
        "season": 2,
        "number": 9,
        "airdate": "2017-09-29",
        "airtime": "11:00",
        "airstamp": "2017-09-29T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1300868"
          }
        }
      },
      {
        "id": 1300873,
        "url": "http://www.tvmaze.com/episodes/1300873/the-lion-guard-2x10-the-ukumbusho-tradition",
        "name": "The Ukumbusho Tradition",
        "season": 2,
        "number": 10,
        "airdate": "2017-10-27",
        "airtime": "11:00",
        "airstamp": "2017-10-27T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1300873"
          }
        }
      },
      {
        "id": 1341831,
        "url": "http://www.tvmaze.com/episodes/1341831/the-lion-guard-2x11-the-bite-of-kenge",
        "name": "The Bite of Kenge",
        "season": 2,
        "number": 11,
        "airdate": "2017-11-03",
        "airtime": "12:00",
        "airstamp": "2017-11-03T16:00:00+00:00",
        "runtime": 30,
        "image": {
          "medium": "http://static.tvmaze.com/uploads/images/medium_landscape/133/334525.jpg",
          "original": "http://static.tvmaze.com/uploads/images/original_untouched/133/334525.jpg"
        },
        "summary": "<p>A ferocious monitor lizard named Kenge bites some of the members of the Lion Guard, temporarily paralyzing them.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1341831"
          }
        }
      },
      {
        "id": 1365445,
        "url": "http://www.tvmaze.com/episodes/1365445/the-lion-guard-2x12-timon-and-pumbaas-christmas",
        "name": "Timon and Pumbaa's Christmas",
        "season": 2,
        "number": 12,
        "airdate": "2017-12-08",
        "airtime": "11:00",
        "airstamp": "2017-12-08T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1365445"
          }
        }
      },
      {
        "id": 1370815,
        "url": "http://www.tvmaze.com/episodes/1370815/the-lion-guard-2x13-the-morning-report",
        "name": "The Morning Report",
        "season": 2,
        "number": 13,
        "airdate": "2018-01-08",
        "airtime": "11:00",
        "airstamp": "2018-01-08T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1370815"
          }
        }
      },
      {
        "id": 1370816,
        "url": "http://www.tvmaze.com/episodes/1370816/the-lion-guard-2x14-the-golden-zebra",
        "name": "The Golden Zebra",
        "season": 2,
        "number": 14,
        "airdate": "2018-01-09",
        "airtime": "11:00",
        "airstamp": "2018-01-09T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1370816"
          }
        }
      },
      {
        "id": 1370817,
        "url": "http://www.tvmaze.com/episodes/1370817/the-lion-guard-2x15-the-little-guy",
        "name": "The Little Guy",
        "season": 2,
        "number": 15,
        "airdate": "2018-01-10",
        "airtime": "11:00",
        "airstamp": "2018-01-10T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1370817"
          }
        }
      },
      {
        "id": 1370818,
        "url": "http://www.tvmaze.com/episodes/1370818/the-lion-guard-2x16-divide-and-conquer",
        "name": "Divide and Conquer",
        "season": 2,
        "number": 16,
        "airdate": "2018-01-11",
        "airtime": "11:00",
        "airstamp": "2018-01-11T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1370818"
          }
        }
      },
      {
        "id": 1438065,
        "url": "http://www.tvmaze.com/episodes/1438065/the-lion-guard-2x17-the-scorpions-sting",
        "name": "The Scorpion's Sting",
        "season": 2,
        "number": 17,
        "airdate": "2018-04-02",
        "airtime": "11:00",
        "airstamp": "2018-04-02T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1438065"
          }
        }
      },
      {
        "id": 1438066,
        "url": "http://www.tvmaze.com/episodes/1438066/the-lion-guard-2x18-the-wisdom-of-kongwe",
        "name": "The Wisdom of Kongwe",
        "season": 2,
        "number": 18,
        "airdate": "2018-04-03",
        "airtime": "11:00",
        "airstamp": "2018-04-03T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1438066"
          }
        }
      },
      {
        "id": 1438067,
        "url": "http://www.tvmaze.com/episodes/1438067/the-lion-guard-2x19-the-kilio-valley-fire",
        "name": "The Kilio Valley Fire",
        "season": 2,
        "number": 19,
        "airdate": "2018-04-04",
        "airtime": "11:00",
        "airstamp": "2018-04-04T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1438067"
          }
        }
      },
      {
        "id": 1438068,
        "url": "http://www.tvmaze.com/episodes/1438068/the-lion-guard-2x20-undercover-kinyonga",
        "name": "Undercover Kinyonga",
        "season": 2,
        "number": 20,
        "airdate": "2018-04-05",
        "airtime": "11:00",
        "airstamp": "2018-04-05T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1438068"
          }
        }
      },
      {
        "id": 1520378,
        "url": "http://www.tvmaze.com/episodes/1520378/the-lion-guard-2x21-cave-of-secrets",
        "name": "Cave of Secrets",
        "season": 2,
        "number": 21,
        "airdate": "2018-09-04",
        "airtime": "11:00",
        "airstamp": "2018-09-04T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1520378"
          }
        }
      },
      {
        "id": 1520379,
        "url": "http://www.tvmaze.com/episodes/1520379/the-lion-guard-2x22-the-zebra-mastermind",
        "name": "The Zebra Mastermind",
        "season": 2,
        "number": 22,
        "airdate": "2018-09-05",
        "airtime": "11:00",
        "airstamp": "2018-09-05T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1520379"
          }
        }
      },
      {
        "id": 1520380,
        "url": "http://www.tvmaze.com/episodes/1520380/the-lion-guard-2x23-the-hyena-resistance",
        "name": "The Hyena Resistance",
        "season": 2,
        "number": 23,
        "airdate": "2018-09-06",
        "airtime": "11:00",
        "airstamp": "2018-09-06T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1520380"
          }
        }
      },
      {
        "id": 1520381,
        "url": "http://www.tvmaze.com/episodes/1520381/the-lion-guard-2x24-the-underground-adventure",
        "name": "The Underground Adventure",
        "season": 2,
        "number": 24,
        "airdate": "2018-09-07",
        "airtime": "11:00",
        "airstamp": "2018-09-07T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1520381"
          }
        }
      },
      {
        "id": 1559314,
        "url": "http://www.tvmaze.com/episodes/1559314/the-lion-guard-2x25-beshte-and-the-beast",
        "name": "Beshte and the Beast",
        "season": 2,
        "number": 25,
        "airdate": "2018-11-12",
        "airtime": "11:00",
        "airstamp": "2018-11-12T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1559314"
          }
        }
      },
      {
        "id": 1595478,
        "url": "http://www.tvmaze.com/episodes/1595478/the-lion-guard-2x26-pride-landers-unite",
        "name": "Pride Landers Unite!",
        "season": 2,
        "number": 26,
        "airdate": "2019-01-21",
        "airtime": "11:00",
        "airstamp": "2019-01-21T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1595478"
          }
        }
      },
      {
        "id": 1595479,
        "url": "http://www.tvmaze.com/episodes/1595479/the-lion-guard-2x27-the-queens-visit",
        "name": "The Queen's Visit",
        "season": 2,
        "number": 27,
        "airdate": "2019-02-18",
        "airtime": "11:00",
        "airstamp": "2019-02-18T16:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1595479"
          }
        }
      },
      {
        "id": 1611653,
        "url": "http://www.tvmaze.com/episodes/1611653/the-lion-guard-2x28-the-fall-of-mizimu-grove",
        "name": "The Fall of Mizimu Grove",
        "season": 2,
        "number": 28,
        "airdate": "2019-03-25",
        "airtime": "11:00",
        "airstamp": "2019-03-25T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1611653"
          }
        }
      },
      {
        "id": 1629012,
        "url": "http://www.tvmaze.com/episodes/1629012/the-lion-guard-2x29-fire-from-the-sky",
        "name": "Fire from the Sky",
        "season": 2,
        "number": 29,
        "airdate": "2019-04-22",
        "airtime": "11:00",
        "airstamp": "2019-04-22T15:00:00+00:00",
        "runtime": 30,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1629012"
          }
        }
      },
      {
        "id": 1688545,
        "url": "http://www.tvmaze.com/episodes/1688545/the-lion-guard-3x01-battle-for-the-pride-lands",
        "name": "Battle for the Pride Lands",
        "season": 3,
        "number": 1,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 55,
        "image": null,
        "summary": "<p>After Kion and Ono are injured in an attempt to defeat Scar in the battle for the Pride Lands, the Lion Guard must embark on a journey to the Tree of Life to help them regain their strength.</p>",
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1688545"
          }
        }
      },
      {
        "id": 1692367,
        "url": "http://www.tvmaze.com/episodes/1692367/the-lion-guard-3x02-the-harmattan",
        "name": "The Harmattan",
        "season": 3,
        "number": 2,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692367"
          }
        }
      },
      {
        "id": 1692368,
        "url": "http://www.tvmaze.com/episodes/1692368/the-lion-guard-3x03-the-accidental-avalanche",
        "name": "The Accidental Avalanche",
        "season": 3,
        "number": 3,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692368"
          }
        }
      },
      {
        "id": 1692369,
        "url": "http://www.tvmaze.com/episodes/1692369/the-lion-guard-3x04-ghost-of-the-mountain",
        "name": "Ghost of the Mountain",
        "season": 3,
        "number": 4,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692369"
          }
        }
      },
      {
        "id": 1692370,
        "url": "http://www.tvmaze.com/episodes/1692370/the-lion-guard-3x05-marsh-of-mystery",
        "name": "Marsh of Mystery",
        "season": 3,
        "number": 5,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692370"
          }
        }
      },
      {
        "id": 1692371,
        "url": "http://www.tvmaze.com/episodes/1692371/the-lion-guard-3x06-dragon-island",
        "name": "Dragon Island",
        "season": 3,
        "number": 6,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692371"
          }
        }
      },
      {
        "id": 1692372,
        "url": "http://www.tvmaze.com/episodes/1692372/the-lion-guard-3x07-journey-of-memories",
        "name": "Journey of Memories",
        "season": 3,
        "number": 7,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692372"
          }
        }
      },
      {
        "id": 1692373,
        "url": "http://www.tvmaze.com/episodes/1692373/the-lion-guard-3x08-the-race-to-tuliza",
        "name": "The Race to Tuliza",
        "season": 3,
        "number": 8,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692373"
          }
        }
      },
      {
        "id": 1692374,
        "url": "http://www.tvmaze.com/episodes/1692374/the-lion-guard-3x09-mama-binturong",
        "name": "Mama Binturong",
        "season": 3,
        "number": 9,
        "airdate": "2019-08-03",
        "airtime": "09:00",
        "airstamp": "2019-08-03T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1692374"
          }
        }
      },
      {
        "id": 1716998,
        "url": "http://www.tvmaze.com/episodes/1716998/the-lion-guard-3x10-friends-to-the-end",
        "name": "Friends to the End",
        "season": 3,
        "number": 10,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1716998"
          }
        }
      },
      {
        "id": 1717000,
        "url": "http://www.tvmaze.com/episodes/1717000/the-lion-guard-3x11-the-tree-of-life",
        "name": "The Tree of Life",
        "season": 3,
        "number": 11,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717000"
          }
        }
      },
      {
        "id": 1717002,
        "url": "http://www.tvmaze.com/episodes/1717002/the-lion-guard-3x12-the-river-of-patience",
        "name": "The River of Patience",
        "season": 3,
        "number": 12,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717002"
          }
        }
      },
      {
        "id": 1717004,
        "url": "http://www.tvmaze.com/episodes/1717004/the-lion-guard-3x13-little-old-ginterbong",
        "name": "Little Old Ginterbong",
        "season": 3,
        "number": 13,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717004"
          }
        }
      },
      {
        "id": 1717005,
        "url": "http://www.tvmaze.com/episodes/1717005/the-lion-guard-3x14-poa-the-destroyer",
        "name": "Poa the Destroyer",
        "season": 3,
        "number": 14,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717005"
          }
        }
      },
      {
        "id": 1717007,
        "url": "http://www.tvmaze.com/episodes/1717007/the-lion-guard-3x15-long-live-the-queen",
        "name": "Long Live the Queen",
        "season": 3,
        "number": 15,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717007"
          }
        }
      },
      {
        "id": 1717008,
        "url": "http://www.tvmaze.com/episodes/1717008/the-lion-guard-3x16-the-lake-of-reflection",
        "name": "The Lake of Reflection",
        "season": 3,
        "number": 16,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717008"
          }
        }
      },
      {
        "id": 1717011,
        "url": "http://www.tvmaze.com/episodes/1717011/the-lion-guard-3x17-triumph-of-the-roar",
        "name": "Triumph of the Roar",
        "season": 3,
        "number": 17,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717011"
          }
        }
      },
      {
        "id": 1717012,
        "url": "http://www.tvmaze.com/episodes/1717012/the-lion-guard-3x18-journey-to-the-pride-lands",
        "name": "Journey to the Pride Lands",
        "season": 3,
        "number": 18,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717012"
          }
        }
      },
      {
        "id": 1717014,
        "url": "http://www.tvmaze.com/episodes/1717014/the-lion-guard-3x19-return-to-the-pride-lands",
        "name": "Return to the Pride Lands",
        "season": 3,
        "number": 19,
        "airdate": "2019-09-02",
        "airtime": "09:00",
        "airstamp": "2019-09-02T13:00:00+00:00",
        "runtime": 22,
        "image": null,
        "summary": null,
        "_links": {
          "self": {
            "href": "http://api.tvmaze.com/episodes/1717014"
          }
        }
      }
    ]
  }
}