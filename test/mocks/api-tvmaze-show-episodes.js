import {rest} from "msw";
import {Urls} from "../../src/constants/Urls";
import {createMockModule, handlerReturnJson} from "../lib/helpers";

export const fixtureNames = {
  DEFAULT: 'default',
  THE_LION_GUARD: 'theLionGuard'
};

const API_TVMAZE_SHOW_EPISODES = createMockModule({
  name: 'API_TVMAZE_SHOW_EPISODES',
  fixtureNames,
  fixtures: {
    [fixtureNames.DEFAULT]: require('../mocks/api-tv-maze/show-episodes--power-puff-girls.js'),
    [fixtureNames.THE_LION_GUARD]: require('../mocks/api-tv-maze/show-episodes--the-lion-guard.js')
  },
  handler: (fixtureName, fixtures) => rest.get(Urls.PowerPuffEpisodes, handlerReturnJson(fixtures[fixtureName]))
});

export default API_TVMAZE_SHOW_EPISODES;