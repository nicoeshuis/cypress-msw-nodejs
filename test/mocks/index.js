import API_TVMAZE_SHOW_EPISODES from "./api-tvmaze-show-episodes";

const mockModules = [
    API_TVMAZE_SHOW_EPISODES
];

export default mockModules;