import API_TVMAZE_SHOW_EPISODES, {fixtureNames} from "../../test/mocks/api-tvmaze-show-episodes";

describe('Testing with MSW', () => {
    it('Test Powerpuff girls', () => {
        cy.visitWithMsw('/');

        cy.get("#root").contains('The Powerpuff Girls')
    });

    it('Test Lion Guard', () => {
        cy.visitWithMsw('/', {
            [API_TVMAZE_SHOW_EPISODES.name]: fixtureNames.THE_LION_GUARD
        });

        cy.get("#root").contains('The Lion Guard')
    });
});