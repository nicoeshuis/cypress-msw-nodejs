import mockModules from "../../test/mocks";

Cypress.Commands.add("visitWithMsw", (route, fixtureOverrides = {}) => {
    cy.setCookie('msw-mocks', mapMocksToCookieValues(fixtureOverrides));
    cy.visit(route);
});

function mapMocksToCookieValues(fixtureOverrides = {}) {
    const cookieValues = mockModules.map(mockModule => {
        const fixtureFile = fixtureOverrides[mockModule.name] || 'default';
        return `${mockModule.name}__${fixtureFile}`;
    });

    return cookieValues.join('&');
}