const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
    entry: {
        "index": path.resolve(__dirname, 'src', 'index.js')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                include: [
                    path.resolve(__dirname, "src"),
                    path.resolve(__dirname, "test"),
                ],
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    },
    devServer: {
        host: 'localhost',
        port: 3000,
        hot: true,
        inline: true,
        proxy: {
            '/': {
                target: 'http://localhost:3001',
                secure: false
            }
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin({
            multiStep: true
        }),
        new CopyPlugin({
            patterns: [
                { from: 'src/assets', to: '.' },
            ],
        }),
    ]
};