import path from 'path'
import fs from 'fs'
import fetch from 'isomorphic-fetch';

// Express deps
import express from 'express'
import cookieParser from 'cookie-parser';

// React deps
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import App from '../src/App'

// MSW deps
import {Urls} from "../src/constants/Urls";
import {setupServer} from "msw/node";
import fixtures from "../test/mocks";
import {getMockMapFromCookieString, updateMswInterceptorWithMockMap} from "../test/lib/helpers";

// Set-up MSW request interceptor
const mswRequestInterceptor = setupServer(...fixtures.map(fixture => fixture.defaultHandler()));
mswRequestInterceptor.listen();

// Set-up express router
const router = express.Router();
router.use('^/$', serverRenderer);

// Set-up express app;
const PORT = 3001;
const app = express();
app.use(cookieParser());
app.use(router);
app.listen(PORT, () => {
    console.log(`SSR running on port ${PORT}`)
});

function serverRenderer(req, res) {
    if(req.cookies['msw-mocks']) {
        updateMswInterceptorWithMockMap(mswRequestInterceptor, getMockMapFromCookieString(req.cookies['msw-mocks']));
    }

    fs.readFile(path.resolve(__dirname, '../src/index.html'), 'utf8', (err, htmlFileContents) => {
        if (err) return sendInternalServerError(err);
        handleRequest(htmlFileContents);
    });

    function handleRequest(htmlFileContents) {
        fetchMovieData()
            .then(movieData => {
                const result = replaceRootWithRenderResult(htmlFileContents, renderAppToString(movieData), {movieData});
                res.send(result)
            })
            .catch(sendInternalServerError)
    }

    function sendInternalServerError(err) {
        console.error(err);
        res.status(500).send('Internal server error');
    }
}

function replaceRootWithRenderResult(htmlFileContents, renderResult, initialState) {
    return htmlFileContents.replace(
        '<div id="root"></div>',
        `<div id="root">${renderResult}</div><script>window.__INITIAL_STATE__=${JSON.stringify(initialState)}</script>`
    )
}

function renderAppToString(movieData) {
    return ReactDOMServer.renderToString(<App movieData={movieData} />);
}

function fetchMovieData() {
    return fetch(Urls.PowerPuffEpisodes)
        .then(response => response.json())
}