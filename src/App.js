import React, {useEffect, useState} from 'react';
import {Urls} from "./constants/Urls";

const App = ({ movieData }) => {
    const [finalMovieData, setFinalMovieData] = useState(movieData);

    // Disable this to see the result of the SSR set-up with MSW
    // useEffect(() => {
    //     fetch(Urls.PowerPuffEpisodes)
    //         .then(response => response.json())
    //         .then(setFinalMovieData);
    // }, []);

    return <>
        {!finalMovieData && <div>Loading</div>}
        {finalMovieData && <div>App {movieData.name} </div>}
    </>
};

export default App;