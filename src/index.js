import React from 'react';
import App from "./App";
import {render} from "react-dom";
import {setUpBrowserMocking} from '../test/lib/msw-browser';

setUpBrowserMocking();

const { movieData } = window.__INITIAL_STATE__ ?? {};

render(<App movieData={movieData} />, document.getElementById('root'));